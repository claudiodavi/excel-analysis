# Instruções #

O software aqui disponível é um projeto de férias entre alunos de Engenharia de Software. O programa tem como objetivo ajudar estudantes de Biotecnologia agilizando o processo de análise de expressão genética.

## Funcionalidades requeridas##

* O software deve ser capaz de ler arquivos .xlsx com mais de 80.000 linhas inseridos pelo usuário.
* O software deverá ler o arquivo reconhecer diferentes grupos de elementos e separá-los adequadamente de acordo com a descrição de uma outra coluna
* O software deve ser capaz de contar quantos elementos há em cada grupo
* O software deve ser capaz de identificar quais os elementos contidos em mais de um grupo. (Teoria dos Conjuntos)

### Requisitos Desejáveis ###

* Leitura de arquivos Diff.
* GUI
* Geração de relatório.
* Gráficos